#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:21985280:578100b495fe3283449e0e955d596d3799819d82; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:16179200:c3ce8c25089be2f10aea2f502c0e2db20e54085e EMMC:/dev/block/bootdevice/by-name/recovery 578100b495fe3283449e0e955d596d3799819d82 21985280 c3ce8c25089be2f10aea2f502c0e2db20e54085e:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
